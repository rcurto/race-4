(function (angular) {
    'use strict';
    angular.module('core').service('atletaCursaFactory', atletaCursaFactory);

    atletaCursaFactory.$inject = ['atletaCursaService'];

    function atletaCursaFactory(atletaCursaService) {
        var cache = [];
        return {
            getById: getById,
            getAll: getAll,
            getByAthlete: getByAthlete,
            getByCoach: getByCoach,
            getNewByCoach: getNewByCoach,
            getByRace: getByRace,
            getCount: getCount,
            store: store,
            getNextRace: getNextRace,
            getAfterNextRace: getAfterNextRace,
            getLastRaceResult: getLastRaceResult,
            getIdsByAthlete: getIdsByAthlete,
            getLastRaceFeedbackByUser: getLastRaceFeedbackByUser,
            getRaceWitOutCoachComment: getRaceWitOutCoachComment

        };

        function getRaceWitOutCoachComment(callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.include('nomcursa');
            query.include('nomcursa.country');

            query.include('user');
            query.notEqualTo('isCoachFull', true);
            query.equalTo('isFull', true);
            //query.descending('nomcursa.data');
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(atletaCursaService(row));
                    });
                    if (callback){
                        callback(rows);
                    }
                    promise.resolve(rows);

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }

        function getLastRaceFeedbackByUser(user, callback) {
            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.include('nomcursa');
            query.include('nomcursa.country');

            query.equalTo('user', user);
            query.equalTo('isCoachFull', true);
            //query.descending('nomcursa.data');
            query.find({
                success: function (results) {
                    if (results.length > 0) {
                        callback(atletaCursaService(results[0]));
                    } else {
                        callback(null);
                    }
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getByRace() {

        }

//getNextRace
        function getLastRaceResult(count, user, callback) {

            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            if (user != undefined) {
                query.equalTo('user', user);
            }
            query.include('nomcursa');
            query.include('nomcursa.country');

            query.include('user');
            query.notEqualTo('temps_segons', null);
            query.notEqualTo('posabs', null);
            query.descending('updatedAt');
            query.limit(count);
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(atletaCursaService(row));
                    });
                    callback(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getAfterNextRace(user, callback) {
            var Curses = new Parse.Query(Parse.Object.extend("Curses"));
            Curses.greaterThan('data', new Date());

            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);

            query.include('nomcursa');
            query.include('nomcursa.country');

            // query.include('nomcursa.data');
            query.matchesKeyInQuery("nomcursa", "objectId", Curses);
            query.ascending('data');
            query.limit(2);
            query.find({
                success: function (results) {
                    if (results.length > 0 && results[1] != undefined) {
                        callback(atletaCursaService(results[1]));
                    } else {
                        callback(null)
                    }
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }




        function getNextRace(user, callback) {
            var promise = new Parse.Promise();

            var Curses = new Parse.Query(Parse.Object.extend("Curses"));
            Curses.greaterThan('data', new Date());
            Curses.ascending('data');
            Curses.containedIn('joined',[user]);
            Curses.limit(1);


            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);

            query.include('nomcursa');
            query.include('nomcursa.country');

            // query.include('nomcursa.data');
            query.matchesKeyInQuery("nomcursa", "objectId", Curses);
            //query.ascending('data');
            query.first().then(function (result) {
                result = result ? atletaCursaService(result) : null;

                if (callback) {
                    callback(result);
                }
                promise.resolve(result);
            });
            return promise;
        }


        /**
         * Get list of upcoming races
         * @returns {Parse.Promise}
         */
        function getNewByCoach() {
            var promise = new Parse.Promise();
            var Curses = Parse.Object.extend("Curses");
            var cursesQuery = new Parse.Query(Curses);
            cursesQuery.greaterThan('data', new Date());
            cursesQuery.ascending('data');


            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.matchesQuery("nomcursa", cursesQuery);

            query.include('nomcursa');
            query.include('nomcursa.country');

            query.include('user');
            query.limit(1000);
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(atletaCursaService(row));
                    });
                    promise.resolve(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;
        }

        function getByCoach(callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            // query.equalTo('user',user.parseObject);
            query.include('nomcursa');
            query.include('nomcursa.country');

            query.include('user');
            query.limit(1000);
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(atletaCursaService(row));
                    });
                    if (callback){
                        callback(rows);
                    }
                    promise.resolve(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }

        function getIdsByAthlete(user, callback) {
            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user.parseObject);
            query.include('nomcursa');
            query.include('nomcursa.country');

            query.limit(1000);
            query.find({
                success: function (results) {
                    var ids = [];
                    angular.forEach(results, function (row) {
                        if (row.get('nomcursa') != undefined) {
                            ids.push(row.get('nomcursa').id);
                        }
                    });
                    callback(ids);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getByAthlete(user, callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user.parseObject);
            query.include('nomcursa');
            query.include('defaultCoachComment');
            query.include('nomcursa.country');

            query.limit(1000);
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(atletaCursaService(row));
                    });
                    if (callback){
                        callback(rows);
                    }
                    promise.resolve(rows);

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }

        function getCount(callback) {
            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.count({
                success: function (count) {
                    callback(count);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function store(data, callback) {
            var parseObject = Parse.Object.extend("atleta_cursa");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }


            store.save(null, {
                success: function (obj) {
                    if (callback != undefined) {
                        callback({success: true});
                    }

                },
                error: function (obj, error) {
                    if (callback != undefined) {
                        callback({success: false, error: error.message});
                    }

                }
            });
        }

        function getById(id, callback) {
            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.include('nomcursa.country');

            query.get(id, {
                success: function (results) {
                    callback(atletaCursaService(results))
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }


        function getAll(callback) {
            var parseObject = Parse.Object.extend("Curses");
            var query = new Parse.Query(parseObject);
            query.include('nomcursa.country');
            query.limit(1000);
            query.find({
                success: function (results) {
                    cache = results;
                    var events = [];
                    angular.forEach(results, function (row) {
                        var eventObject = atletaCursaService(row);
                        events.push(eventObject);
                    });
                    callback(events);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });


        }


    }
})(angular);

