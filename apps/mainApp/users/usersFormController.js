(function (angular) {
    'use strict';

    angular.module('gameApp').
        controller('userFormController', userFormController);

    userFormController.$inject = ['$scope', 'userFactory', '$location', '$routeParams', '$rootScope'];
    function userFormController($scope, userFactory, $location, $routeParams, $rootScope) {


        $scope.env = {
            user_id: $routeParams.id,
            loading: true,
            saving: false,
            model: {
            },
            hotels: [],
            error: null
        };

        userFactory.auth(function (user) {
            $scope.env.user = user;
            $scope.$apply()
        });

        if ($scope.env.user_id != undefined) {
            //  loading user data
            userFactory.getUserById($scope.env.user_id, function (user) {
                $scope.env.loading = false;
                $scope.env.model = user.attributes;
                $scope.env.user = user;
                $scope.$apply();
            });
        }


        $scope.save = function (data) {
            $scope.env.saving = true;
            if ($scope.env.user_id != undefined) {
                $scope.env.user.update(data, function (result) {
                    $scope.env.saving = false;
                    if (result.success === false) {
                        $scope.env.error = result.error;
                        $scope.$apply();
                    } else {
                        $location.path('/users');
                        $scope.$apply();
                    }
                });
            } else {
                userFactory.store(data, function (result) {
                    $scope.env.saving = false;
                    if (result.success === false) {
                        $scope.env.error = result.error;
                        $scope.$apply();
                    } else {
                        $scope.$apply(function () {
                            $location.path('/users');
                        });
                    }
                })
            }


        }

    }
})(angular);