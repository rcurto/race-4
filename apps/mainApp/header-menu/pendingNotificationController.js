(function () {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('pendingNotificationController', pendingNotificationController);

    pendingNotificationController.$inject = ['$scope', 'userFactory'];

    function pendingNotificationController($scope, userFactory) {

        $scope.env = {
            user: undefined,
            pendingNotificationList: null
        };

        userFactory.auth(function (user) {
            $scope.env.user = user;

            userFactory.getPendingNotifications(function (pendingNotificationList) {

                if (user.get("startTrain") && userFactory.getChkNotificationForNextTraning(user.get("startTrain")) == true) {

                    $scope.env.nextTrainingNotify = true;
                    var pStartTrain = new Date(user.get("startTrain"));

                    pStartTrain.setDate(pStartTrain.getDate() + 13);

                    var pTimeStamp = pStartTrain.getTime();
                    var pNextTraningInterval = userFactory.pGetIntervalDate(pTimeStamp);

                    pendingNotificationList.push({
                        objectId: "",
                        noticeMsg: "Remember to write your comments for the next training plan",
                        intervalMinute: pNextTraningInterval,
                        coachComment: "x`",
                        timestampe: pTimeStamp,
                        tblName: "",
                        icon: "icon-notebook",
                        noticeTitle: "",
                        multishow: "1",
                        alink: "feedbacks",
                        showMsg: false,
                        removable: false
                    });
                }
                $scope.env.pendingNotificationList = pendingNotificationList;
                $scope.$apply();

                HeaderUIToastr.init();
            });

        });

    }

})();