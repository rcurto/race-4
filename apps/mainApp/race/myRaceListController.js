(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('myRaceListController', myRaceListController);
    myRaceListController.$inject = ['$scope', 'atletaCursaFactory', '$window', '$timeout', '$filter', 'xmlExport'];
    function myRaceListController($scope, atletaCursaFactory, $window, $timeout, $filter, xmlExport) {
        $scope.$parent.initPage = init; // start point

        $scope.env = {
            loading: true,
            posts: [],
            postsSource: [],
            tableConfig: {
                itemsPerPage: 50,
                fillLastPage: false
            },
            filters: {
                nom: null,
                printData: null,
                season: null,
                trainingPlan: undefined,
                username: undefined,

            },
            seasons: []
        };
        var promises = [];
        $scope.user = null;

        function init() {
            var user = $scope.$parent.user;
            $scope.user = user;
            if (user.isAthlete()) {
                var racePromise = atletaCursaFactory.getByAthlete(user).then(function (posts) {
                    $scope.env.posts = posts;
                    $scope.env.postsSource = posts;
                    for (var i in posts) {
                        if ($scope.env.seasons.indexOf(posts[i].season) == -1) {
                            if (posts[i].season != undefined)
                                $scope.env.seasons.push(posts[i].season)
                        }
                    }
                });
                promises.push(racePromise);

            } else if (user.isCoach()) {
                var racePromise = atletaCursaFactory.getByCoach().then(function (posts) {
                    $scope.env.posts = posts;
                    $scope.env.postsSource = posts;
                    for (var i in posts) {
                        if ($scope.env.seasons.indexOf(posts[i].season) == -1) {
                            if (posts[i].season != undefined)
                                $scope.env.seasons.push(posts[i].season)
                        }
                    }
                });
                promises.push(racePromise);
            }
            Parse.Promise.when(promises).then(function () {
                pageLoaded();
            });
        }

        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }

        $scope.remove = function remove(post, index) {
            var confirmAction = confirm($filter('translate')('Delete?'));
            if (!confirmAction) {
                return;
            }
            $scope.env.postsSource = $scope.env.postsSource.filter(function (row) {
                if (row.getId() != post.getId()) {
                    return true;
                }
                return false;
            });
            $scope.env.posts.splice(index, 1);
            post.delete();
        };


        $scope.export = function () {
            var headers = [
                $filter('translate')('Date'),
                $filter('translate')('Race'),
                $filter('translate')('Km'),
                $filter('translate')('+Slope'),
                $filter('translate')('City'),
                $filter('translate')('Zone'),
                $filter('translate')('Country'),
                $filter('translate')('Name'),
                $filter('translate')('Plan'),
                $filter('translate')('Ranking'),
                $filter('translate')('Time'),
                $filter('translate')('Feelings'),
                $filter('translate')('Message for the athlete'),
                $filter('translate')('Action')
            ];
            var json = [];
            for (var i in $scope.env.posts) {
                var post = $scope.env.posts[i]
                json.push([
                    post.getCurseData(),
                    post.get('nomcursa').get('nom'),
                    post.get('nomcursa').get('distancia'),
                    post.get('nomcursa').get('desnivell'),
                    post.get('nomcursa').get('poblacio'),
                    post.get('nomcursa').get('provincia'),
                    post.get('nomcursa').get('pais'),
                    post.get('user').get('username'),
                    post.get('user').get('trainingPlan'),
                    post.posabs,
                    post.temps_segons,
                    post.feelings,
                    post.beforeCoachComment,
                    post.beforeCoachComment
                ])
            }

            var xml = xmlExport.getXml(headers, json);
            var fileName = "curses.xls";

            var uri = 'data:text/xls;charset=utf-8,' + encodeURI(xml);
            var link = document.createElement("a");
            link.href = uri;

            link.style = "visibility:hidden";
            link.download = fileName;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        $scope.print = function () {
            $timeout(function () {
                $window.print();
            }, 100)
        };

        $scope.saveResult = function (post) {
            var savingFields = {
                feelings: post.feelings,
                temps_segons: post.temps_segons,
                posabs: post.posabs,
                rating: post.rating,
                comments: post.comments,
                isFull: true
            };
            post.update(savingFields);
            post.isFull = true;
            alertify.success($filter('translate')('Race feedback saved'));
        };

        $scope.unjoin = function (post, index) {
            alertify.confirm($filter('translate')('Un Join?'), function (e, str) {
                if (e) {
                    var race = post.get('nomcursa');
                    var joined = post.get('nomcursa').get('joined');

                    var joinedCleared = [];
                    angular.forEach(joined, function (user) {
                        if (user.id != $scope.user.getId()) {
                            joinedCleared.push(user)
                        }
                    });
                    if (joinedCleared.length == 0) {
                        race.unset('joined');
                    } else {
                        race.set('joined', joinedCleared);
                    }

                    race.save();

                    $scope.env.posts = $scope.env.posts.filter(function (selPost) {
                        if (selPost.getId() == post.getId()) {
                            return false;
                        }
                        return true;
                    });
                    post.delete();

                    alertify.success($filter('translate')('Race deleted'));
                    $scope.$apply();
                }
            });


        };

        $scope.$watchCollection('env.filters', function (value) {
            var data = null;
            var season = null;
            var trainingPlan = null;
            var name = $scope.env.filters.nom ? $scope.env.filters.nom.toLowerCase() : null;
            var username = $scope.env.filters.username ? $scope.env.filters.username.toLowerCase() : null;

            if ($scope.env.filters.printData) {
                data = $filter("date")($scope.env.filters.printData, 'dd-MM-yyyy');
            }
            if ($scope.env.filters.season) {
                season = $scope.env.filters.season
            }

            if ($scope.env.filters.trainingPlan) {
                trainingPlan = $scope.env.filters.trainingPlan
            }
            $scope.env.posts = $scope.env.postsSource.filter(function (value) {

                if (name != null && value.get('nomcursa').get('nom').toLowerCase().indexOf(name) == -1) {
                    return false;
                }

                if (data != null && value.printData.indexOf(data) == -1) {
                    return false;
                }
                if (season != null && value.season != season) {
                    return false;
                }
                if (trainingPlan != null && value.get('user').get('trainingPlan') != trainingPlan) {
                    return false;
                }
                if (username != null && value.user && value.user.username && value.user.username.toLowerCase().indexOf(username) == -1) {
                    return false;
                }
                if (username != null && !value.user) {
                    return false;
                }


                return true;
            })

        });

    }
})(angular);