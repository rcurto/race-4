(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('raceListController', raceListController);

    raceListController.$inject = ['$scope', 'cursesFactory', '$filter', 'circuitsFactory', 'countryFactory'];
    function raceListController($scope, cursesFactory, $filter, circuitsFactory, countryFactory) {

        $scope.env = {
            loading: true,
            posts: [],
            postsSource: [],
            tableConfig: {
                itemsPerPage: 50,
                fillLastPage: false
            },
            filters: {},
            joins: [],
            user: null,
            circuits: []

        };
        $scope.config = {
            itemsPerPage: 50,
            fillLastPage: true
        };


        $scope.$parent.initPage = init; // start point

        var promises = [];
        $scope.user = null;

        function init() {
            $scope.user = $scope.$parent.user;
            var user = $scope.user;

            var circuitPromise = circuitsFactory.getAll().then(function (posts) {
                $scope.env.circuits = posts;
                $scope.$apply();
            });
            promises.push(circuitPromise);

            var countryPromise = countryFactory.getAll().then(function (posts) {
                $scope.env.country = posts;
                $scope.$apply();
            });
            promises.push(countryPromise);


            var postsPromise = cursesFactory.getAll().then(function (posts) {
                $scope.env.loading = false;
                $scope.env.posts = posts;
                $scope.env.postsSource = posts;
                $scope.$apply();
            });
            promises.push(postsPromise);


            var coursesIdsPromise = user.getJoinCoursesIds().then(function (ids) {
                $scope.env.joins = ids;
                $scope.$apply();
            });
            promises.push(coursesIdsPromise);

            Parse.Promise.when(promises).then(function () {
                pageLoaded();
            });

        }


        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }


        $scope.remove = function remove(post, index) {
            var confirmAction = confirm($filter('translate')('Delete?'));
            if (!confirmAction) {
                return;
            }
            $scope.env.postsSource = $scope.env.postsSource.filter(function (row) {
                if (row.getId() != post.getId()) {
                    return true;
                }
                return false;
            });
            $scope.env.posts.splice(index, 1);
            post.delete();
        };


        $scope.$watchCollection('env.filters', function () {
            var data = null;
            var country_id = null;
            var zone_id = null;
            var circuit_id = null;
            var categoria = $scope.env.filters.categoria ? $scope.env.filters.categoria : null;
            var month = $scope.env.filters.month ? $scope.env.filters.month : null;
            var name = $scope.env.filters.nom ? $scope.env.filters.nom.toLowerCase() : null;


            if ($scope.env.filters.country) {
                country_id = $scope.env.filters.country.getId();
            }
            if ($scope.env.filters.zone) {
                zone_id = $scope.env.filters.zone.id;
            }
            if ($scope.env.filters.circuit) {
                circuit_id = $scope.env.filters.circuit.getId();
            }
            if ($scope.env.filters.printData) {
                data = $filter("date")($scope.env.filters.printData, 'dd-MM-yyyy');
            }
            if (month) {
                var beginDate = moment().month(month*1).date(1);
                var endDate = moment().month(month*1+1).date(1).subtract(1, 'days');

            }

            $scope.env.posts = $scope.env.postsSource.filter(function (value) {
                if (name != null && value.nom.toLowerCase().indexOf(name) == -1) {
                    return false;
                }

                if (country_id != null && value.countryId != country_id) {
                    return false;
                }
                if (zone_id != null && value.zoneId != zone_id) {
                    return false;
                }

                if (data != null && value.printData.indexOf(data) == -1) {
                    return false;
                }

                if (categoria != null && value.categoria != categoria) {
                    return false;
                }
                if (circuit_id != null && value.circuitId != circuit_id) {
                    return false;
                }
                if ( beginDate && endDate && !moment(value.printData,'DD-MM-YYYY').isBetween(beginDate, endDate)  ){
                    return false;
                }

                return true;
            })

        });

    }
})(angular);